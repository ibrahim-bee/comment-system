<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model {

    protected $table = 'comments';

    function user()
	{
		return $this->hasOne('App\User' , "id" , 'user_id')->select();
	}
	function children()
	{
	    return $this->hasMany(self::class, 'parent_id')->with('user');
	}
}