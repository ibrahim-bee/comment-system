<?php 
namespace App\Http\Controllers\Comments;

use App\Http\Controllers\Controller;
use App\Comments;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
	public function view()
	{
		$root_id = 1;
		$comments = new Comments;
		$comments = $comments->with('children')->with('user')
					->where("root_id",$root_id)
					->whereNull("parent_id")
					->get()->toArray();
		// print_r($comments);
		return view('comments.default')->with("comments",$comments)->with("root_id",$root_id);
	}
	
	public function postComment()
	{
		$comment = new Comments;
		$comment->root_id = Input::get('root_id');
		$comment->user_id = Auth::user()->id;
		$comment->comment = Input::get('comment');
		if (Input::has('parent_id')) 
		{
			$comment->parent_id = Input::get('parent_id');
		}
		if($comment->save())
			return 1;
		else
			return 0;
	}

	public function editComment()
	{
		$comment = Comments::find(Input::get('comment_id'));
		$comment->comment = Input::get("edited_comment");
		if($comment->save())
			return 1;
		else
			return 0;
	}

	public function deleteComment()
	{
		$comment = Comments::find(Input::get('comment_id'));
		if ($comment->delete()) {
			return 1;
		}
		else
			return 0;
	}
}
