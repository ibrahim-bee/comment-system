<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="_token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicon.ico">

    <title>comment system</title>

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,300italic,400italic,600italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic' rel='stylesheet' type='text/css'>
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="css/main.css" rel="stylesheet">

  </head>

  <body>
    <!-- container -->
    <div class="container page">
      
      <div class="row">
        <div class="col-md-12">
           <!-- <h1>Scientific Forum</h1> -->
        </div>
      </div>
            
      <!-- MODULE INFO -->
      <div class="row">
        <div class="col-md-12 module-list forum">    
            <a href="modules_details.html"><img src="images/pic_modules_1.jpg" width="360" height="270" class="news"></a>
            <h2>Topic 1</h2>
            <p class="text-justify margin-top-medium">TOPIC ONE!</p>
            <hr>
            <form  method="post" >
              <input type="hidden" value="{{$root_id}}" id='root_id' name='root_id'></input>
              <input type="hidden" value="wDePH186Lz" id='token' name='token'></input>
              <div class="add-comment">
                <h4>Add a comment</h4>
                <div>
                  <textarea class="form-control" id = "comment" name="comment" placeholder="" rows="2" required></textarea>
                </div>
                <p class="text-right">
                  <input type="submit" value ="Comment" class="btn margin-top-small" id="other">
               </p>
              </div>
            </form>
            <hr>
            <div class="add-comment margin-bottom-large">
                <h2>Comments</h2>
                
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="comments">
                    @foreach($comments as $comment)
                      <table class="table bg-white no-border">
                          <tbody>
                                <tr>
                                    <td class="no-border td-small">
                                    <img class="tb-img" src="images/no-image-profile.png">
                                    </td>
                                    <td class="no-border td-right">
                                        <h4 class="orange text-left">{{$comment['user']['name']}}</h4>
                                        <p class="text-left post-date">{{$comment['created_at']}}</p>
                                        <p class="text-left" id="com{{$comment['id']}}">{{$comment['comment']}}
                                        </p>
                                        @if( Auth::user()->id
                                         ==  $comment['user']['id'])
                                          <button onclick="delete_comment({{$comment['id']}})"> Delete</button>
                                          <button onclick="edit_form({{$comment['id']}},
                                          '{{$comment['comment']}}' )"> Edit</button>
                                          @endif
                                        <div class="add-comment-q">

                                          <div class='panel' id="slidepanel">
                                            <form  method="post" >
                                              <input type="hidden" value="{{$root_id}}" id='root_id' name='root_id'></input>
                                              <input type="hidden" value="wDePH186Lz" id='token' name='token'></input>
                                              <input type="hidden" value="{{$comment['id']}}" id='parent_id' name='parent_id'></input>
                                              <textarea class="form-control" id = "comment" name="comment" placeholder="" rows="2" required></textarea>
                                            </div>
                                            <p class="text-right">
                                              <input type="submit" value ="Reply" class="btn margin-top-small" id="other">
                                            </p>
                                          </div>
                                        </form>                                        
                                      </div>
                                    </div>
                                          <hr>
                                          @foreach($comment['children'] as $sub_comment)
                                            <table class="table bg-white no-border">
                                              <tbody>
                                                <tr>
                                                  <td class="no-border td-small">
                                                    <img class="tb-img" src="images/no-image-profile.png">
                                                  </td>
                                                  <td class="no-border td-right">
                                                    <h4 class="orange text-left">{{$sub_comment['user']['name']}}</h4>
                                                    <p class="text-left post-date">{{$sub_comment['created_at']}}</p>
                                                    <p class="text-left">{{$sub_comment['comment']}}</p>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          @endforeach
                      
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        @endforeach
                        
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
   <script src="/js/comments.js"></script>
  </body>
</html>
