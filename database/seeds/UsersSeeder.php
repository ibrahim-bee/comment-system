<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder{

    public function run(){
        DB::table('users')->delete();
        $user = User::create(array(
            'name'    => 'John',
            'email'     => 'ibrhaim@gmail.com',
            'password'      => Hash::make('123456')
        ));
        $user = User::create(array(
            'name'    => 'Smith',
            'email'     => 'smith@gmail.com',
            'password'      => Hash::make('123456')
        ));
    }
}