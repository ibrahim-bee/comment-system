<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/login', 'Auth\AuthController@getLogin');
Route::post('/login','Auth\AuthController@postLogin');

Route::get('/home', 'Comments\CommentsController@view');
Route::post('/post_comment', 'Comments\CommentsController@postComment');
Route::post('/change_comment','Comments\CommentsController@editComment');
Route::delete('/delete_comment', 'Comments\CommentsController@deleteComment');
