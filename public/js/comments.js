$("form").submit(function( event ) 
{
  event.preventDefault();
  var form = $( this );
  $.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    }
  });
  $.ajax({
    method: 'POST',
    url: '/post_comment/',
    data: form.serialize(),
    dataType: 'json',
    success: function( resp ) 
    {
        if (resp == '0') 
        {   
            alert("Try again please.");
        }
        else
        {
            alert("page will refresh to see your comment.");
            location.reload();
            
      }
  }
});
}
);


function delete_comment(comment_id)
{
    $.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('meta[name="_token"]').attr('content')
    }
});
  $.ajax({
    method: 'DELETE',
    url: '/delete_comment/',
    data: {'comment_id':comment_id},
    dataType: 'json',
    success: function( resp ) 
    {
        if (resp == '0') 
        {   
            alert("Try again please.");
        }
        else
        {
            alert("comment deleted.");
            location.reload();
        }
    }
    });

}


function change_comment(div)
{
        id = ($(div+" #comment_id").val());
        comment = ($(div+" #edited_comment").val());

        $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="_token"]').attr('content')
        }
      });
      $.ajax({
        method: 'POST',
        url: '/change_comment/',
        data: {"comment_id":id,"edited_comment":comment},
        dataType: 'json',
        success: function( resp ) 
        {
            if (resp == '0') 
            {   
                alert("Try again please.");
            }
            else
            {
                alert("page will refresh to see your comment.");
                location.reload();
          }
      }
    });
}

function edit_form(id,comment)
{   
    edit_form = '<input type="hidden" value="'
                +id+
                '" id="comment_id" name="comment_id"></input><textarea class="form-control" id = "edited_comment" name="edited_comment" placeholder="" rows="2" required>'
                +comment+
                '</textarea> </div> <p class="text-right"><button onclick="change_comment(\'#com'+id+'\')"  class="btn margin-top-small" id="other">Done</button></p></div> ';

            $("#com"+id).html(edit_form);
}

